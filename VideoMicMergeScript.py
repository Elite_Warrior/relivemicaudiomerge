import moviepy.editor as mp
import moviepy.audio.AudioClip as ac
import os
import msvcrt

VERSION = 1.11

class VideoMicMergeScript:    
    
    def __init__(self) -> None:
        pass
    
    def get_bigger_number(self, num_a, num_b):
        if num_a >= num_b:
            return num_a
        else:
            return num_b
    
    def mix_audio(self, video, audio, factor=8):
        va = video.audio
        na = audio
        a_arr = audio.to_soundarray()
        for index in range(len(a_arr)): # Mono auf Stereo
            a_arr[index][1] = a_arr[index][0]
        na = ac.AudioArrayClip(a_arr, fps=44100)
        na_max = na.max_volume()
        va_max = va.max_volume()
        
        if va_max*factor < na_max:
            va = va.volumex(na_max/va_max/factor)
        
        full_factor = 1 / self.get_bigger_number(na_max, va_max) # * 0.5
        na = na.volumex(full_factor)
        va = va.volumex(full_factor)
        na_max = na.max_volume()
        va_max = va.max_volume()
        
        if va_max > na_max*factor:
            return va # Mic Spur ist zu leise, deshalb nur Video Audio als Ausgang
        else: 
            mixed_audio = mp.CompositeAudioClip([va, na])
            return mixed_audio

    def get_replay_files(self, path_to_files):
        file_list = list()
        # listet alle Dateien mit zuzsammengehöriger .m4a Spur
        for files in os.listdir('.'):
            if os.path.isfile(files):
                file_split = files.rsplit('.', maxsplit=1)
                if file_split[0].startswith('Replay_') and file_split[1] == 'mp4':
                    if os.path.isfile(file_split[0]+'.m4a'):
                        file_list.append(files)
        file_list.reverse()
        return file_list

    def choose_file(self, file_list):
        max = len(file_list)
        if max < 1:
            raise Exception('Keine Videos entdeckt')
        if max > 9:
            max = 9
        for index in range(0, max):
            print('[{}] {}'.format(index, file_list[index]))
        print("Bitte Videonummer eintippen: ", end='')
        input_char = msvcrt.getch()
        if input_char == chr(27).encode():
            raise Exception('Programm abgebrochen')
        if not input_char.isdigit():
            raise Exception('Keine Zahl erkannt')
        return file_list[int(input_char)]
            
    def combine_video_audio(self, file_input):
        video = mp.VideoFileClip(file_input+'.mp4')
        audio = mp.AudioFileClip(file_input+'.m4a')
        mixed_audio = self.mix_audio(video, audio)
        video = video.set_audio(mixed_audio)
        video.audio.duration = video.duration
        video_name = 'MicMixed_'+file_input+'.mp4'
        print('Bitte Start- und Endzeitpunkt eintippen, kann auch leer gelassen werden')
        sub_start = input('Clipstart in Sek.: ')
        sub_end = input('Clipende in Sek.: ')
        if sub_start == '' or sub_start == '0':
            sub_start = 0
        if sub_end == '' or sub_end == '0':
            sub_end = None
        video.subclip(sub_start, sub_end).write_videofile(video_name, bitrate="10000000") # 10.000k Bit/s
        return video_name


def main():
    print("ReLive Microfone Merge Script " + str(VERSION) + "v")
    try:
        mix_script = VideoMicMergeScript()
        file_list = mix_script.get_replay_files('.')
        chosen_file = mix_script.choose_file(file_list)
        file_name = chosen_file.rsplit('.', maxsplit=1)[0]
        print(file_name)
        video_name = mix_script.combine_video_audio(file_name)
        print(video_name + ' wurde erstellt')
    except Exception as e:
        print(str(e))
    input('Bitte ENTER drücken um das Programm zu beenden')
    

if __name__ == '__main__':
    main()